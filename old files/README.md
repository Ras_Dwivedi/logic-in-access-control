# Logic in access control
Execute the file CDD.py in folder crypt to run the code.  You might have to install some libraries. Use RSA.py to generate public and private key pair and to store them. 

In general:

use & for AND 

use | for OR 

use -> for implication

rule  0: EXCLUDED_MIDDLE COLON NOT NOT expression

rule  1: AND_INTRODUCTION COLON (ANDI)expression COMMA expression 

rule  2: AND_ELIMINATION COLON (ANDE) expression AND expression COMMA expression

rule  3: MODES_PONES COLON (modes_pones)expression IMPLIES expression COMMA expression

rule  4: MODES_TOLLENS (modes_tollens) COLON expression IMPLIES expression

rule  5: SYLLOGISM (syl) COLON expression IMPLIES expression COMMA expression IMPLIES expression

rule  6: OR_INTRODUCTION (ORI) COLON expression IMPLIES expression OR expression

rule  7: OR_ELIMINATION (ORE) COLON expression OR expression COMMA expression IMPLIES FALSE

rule  8: OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression

rule  9: UNITM COLON expression COMMA PRINCIPLE SAYS expression

rule 10: BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rule

rule 11: ASSUME expression COMMA rule
