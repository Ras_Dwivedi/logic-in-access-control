
# parsetab.py
# This file is automatically generated. Do not edit.
# pylint: disable=W,C,R
_tabversion = '3.10'

_lr_method = 'LALR'

_lr_signature = 'leftIMPLIESleftORleftANDrightSAYSAND AND_ELIMINATION AND_INTRODUCTION ASSUME BINDM CAN_ACCESS COLON COMMA CONTROLS DERIVES DOT EQUALS EQUIVALENT EXCLUDED_MIDDLE FALSE FORALL F_SLASH IMPLIES LPAREN L_SQ_BRACKET MODES_PONES MODES_TOLLENS NOT OBJECT OR OR_ELIMINATION OR_INTRODUCTION PRINCIPLE PROPOSITION RPAREN R_SQ_BRACKET SAYS SAYS_FOR SEMICOLON SYLLOGISM TAPP TLAM TRUE UNITM VARIABLEstatement : expressionstatement : ruleexpression : expression AND expressionexpression : expression IMPLIES expressionexpression : expression OR expressionexpression : PRINCIPLE SAYS expressionexpression : LPAREN expression RPARENexpression :  OBJECTexpression : TRUEexpression : NOT expressionrule : EXCLUDED_MIDDLE COLON expressionrule : AND_INTRODUCTION COLON expression COMMA expressionrule : AND_ELIMINATION COLON expression COMMA expressionrule : MODES_PONES COLON expression IMPLIES expression COMMA expressionrule : MODES_TOLLENS COLON expression IMPLIES expressionrule : SYLLOGISM COLON expression IMPLIES expression COMMA expression IMPLIES expressionrule : OR_INTRODUCTION COLON expression COMMA expression OR expressionrule : OR_ELIMINATION COLON expression OR expression COMMA NOT expressionrule : OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expressionrule : UNITM COLON expression COMMA PRINCIPLE SAYS expressionrule : BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rulerule : ASSUME expression COMMA  rule'
    
_lr_action_items = {'SAYS':([2,46,68,],[20,60,76,]),'OBJECT':([0,5,14,17,20,21,23,24,25,26,27,28,31,33,34,35,36,53,55,56,57,58,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,]),'COLON':([3,6,7,9,10,11,12,13,16,18,],[21,23,24,25,26,27,28,29,31,33,]),'PRINCIPLE':([0,5,14,17,20,21,23,24,25,26,27,28,29,31,33,34,35,36,53,55,56,57,58,59,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[2,2,2,2,2,2,2,2,2,2,2,2,46,2,2,2,2,2,2,2,2,2,2,68,2,2,2,2,2,2,2,2,2,2,2,2,2,2,]),'OR_INTRODUCTION':([0,39,91,],[3,3,3,]),'TRUE':([0,5,14,17,20,21,23,24,25,26,27,28,31,33,34,35,36,53,55,56,57,58,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,]),'RPAREN':([1,4,30,32,37,47,50,51,52,],[-8,-9,47,-10,-6,-7,-3,-4,-5,]),'ASSUME':([0,39,77,91,],[5,5,84,5,]),'MODES_TOLLENS':([0,39,91,],[6,6,6,]),'MODES_PONES':([0,39,91,],[7,7,7,]),'COMMA':([1,4,22,32,37,38,45,47,48,49,50,51,52,65,66,67,69,88,90,],[-8,-9,39,-10,-6,53,59,-7,61,62,-3,-4,-5,73,74,75,77,91,92,]),'EXCLUDED_MIDDLE':([0,39,91,],[9,9,9,]),'AND_ELIMINATION':([0,39,91,],[16,16,16,]),'$end':([1,4,8,15,19,32,37,42,47,50,51,52,54,64,70,71,78,79,83,86,89,93,96,],[-8,-9,0,-2,-1,-10,-6,-11,-7,-3,-4,-5,-22,-15,-13,-12,-17,-14,-20,-18,-16,-21,-19,]),'OR_ELIMINATION':([0,39,91,],[11,11,11,]),'IMPLIES':([1,4,19,22,30,32,37,38,40,41,42,43,44,45,47,48,49,50,51,52,63,64,65,66,67,69,70,71,78,79,80,82,83,86,88,89,90,94,96,],[-8,-9,35,35,35,35,-6,35,55,56,35,57,35,35,-7,35,35,-3,-4,-5,35,-4,-4,-4,-5,35,35,35,-5,35,85,87,35,35,35,-4,-4,95,-4,]),'UNITM':([0,39,91,],[12,12,12,]),'BINDM':([0,39,91,],[13,13,13,]),'LPAREN':([0,5,14,17,20,21,23,24,25,26,27,28,31,33,34,35,36,53,55,56,57,58,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,]),'AND':([1,4,19,22,30,32,37,38,40,41,42,43,44,45,47,48,49,50,51,52,63,64,65,66,67,69,70,71,78,79,80,82,83,86,88,89,90,94,96,],[-8,-9,34,34,34,34,-6,34,34,34,34,34,34,34,-7,34,34,-3,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,34,]),'SYLLOGISM':([0,39,91,],[10,10,10,]),'NOT':([0,5,14,17,20,21,23,24,25,26,27,28,31,33,34,35,36,53,55,56,57,58,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,81,17,17,17,17,17,17,17,]),'AND_INTRODUCTION':([0,39,91,],[18,18,18,]),'OR':([1,4,19,22,30,32,37,38,40,41,42,43,44,45,47,48,49,50,51,52,63,64,65,66,67,69,70,71,78,79,80,82,83,86,88,89,90,94,96,],[-8,-9,36,36,36,36,-6,36,36,36,36,36,58,36,-7,36,36,-3,36,-5,72,36,36,36,-5,36,36,36,-5,36,36,36,36,36,36,36,36,36,36,]),}

_lr_action = {}
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = {}
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'expression':([0,5,14,17,20,21,23,24,25,26,27,28,31,33,34,35,36,53,55,56,57,58,60,61,62,72,73,74,75,76,81,84,85,87,92,95,],[19,22,30,32,37,38,40,41,42,43,44,45,48,49,50,51,52,63,64,65,66,67,69,70,71,78,79,80,82,83,86,88,89,90,94,96,]),'rule':([0,39,91,],[15,54,93,]),'statement':([0,],[8,]),}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
   for _x, _y in zip(_v[0], _v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = {}
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> statement","S'",1,None,None,None),
  ('statement -> expression','statement',1,'p_statement_expr','CDD.py',402),
  ('statement -> rule','statement',1,'p_statement_rule','CDD.py',421),
  ('expression -> expression AND expression','expression',3,'p_expression_and','CDD.py',434),
  ('expression -> expression IMPLIES expression','expression',3,'p_expression_implies','CDD.py',440),
  ('expression -> expression OR expression','expression',3,'p_expression_or','CDD.py',446),
  ('expression -> PRINCIPLE SAYS expression','expression',3,'p_expression_says','CDD.py',454),
  ('expression -> LPAREN expression RPAREN','expression',3,'p_expression_group','CDD.py',477),
  ('expression -> OBJECT','expression',1,'p_expression_object','CDD.py',484),
  ('expression -> TRUE','expression',1,'p_expression_true','CDD.py',491),
  ('expression -> NOT expression','expression',2,'p_expression_not','CDD.py',512),
  ('rule -> EXCLUDED_MIDDLE COLON expression','rule',3,'p_rule_EXCLUDED_MIDDLE','CDD.py',559),
  ('rule -> AND_INTRODUCTION COLON expression COMMA expression','rule',5,'p_rule_and_INTRODUCTION','CDD.py',573),
  ('rule -> AND_ELIMINATION COLON expression COMMA expression','rule',5,'p_rule_and_elimination','CDD.py',592),
  ('rule -> MODES_PONES COLON expression IMPLIES expression COMMA expression','rule',7,'p_rule_modes_pones','CDD.py',615),
  ('rule -> MODES_TOLLENS COLON expression IMPLIES expression','rule',5,'p_rule_modes_tollens','CDD.py',637),
  ('rule -> SYLLOGISM COLON expression IMPLIES expression COMMA expression IMPLIES expression','rule',9,'p_rule_syllogism','CDD.py',665),
  ('rule -> OR_INTRODUCTION COLON expression COMMA expression OR expression','rule',7,'p_rule_or_introduction','CDD.py',686),
  ('rule -> OR_ELIMINATION COLON expression OR expression COMMA NOT expression','rule',8,'p_rule_or_elemination1','CDD.py',703),
  ('rule -> OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression','rule',13,'p_rule_or_elemination2','CDD.py',724),
  ('rule -> UNITM COLON expression COMMA PRINCIPLE SAYS expression','rule',7,'p_rule_unitm','CDD.py',764),
  ('rule -> BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rule','rule',10,'p_rule_bindm','CDD.py',781),
  ('rule -> ASSUME expression COMMA rule','rule',4,'p_rule_Implication_Introduction','CDD.py',807),
]
