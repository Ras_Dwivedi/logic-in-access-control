# import ecdsa

# # SECP256k1 is the Bitcoin elliptic curve
# sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1) 
# vk = sk.get_verifying_key()
# while True:
# 	print("\nPublic key is:	"+ str(vk))
# 	print("\nSecret key is:	"+ str(sk))
# 	message=raw_input('\nEnter your message here \n ...')
# 	sig = sk.sign(message)
# 	print("signed message is: \n" + sig)
# 	print(vk.verify(sig, message))

# from ecdsa import SigningKey
# import binascii
# sk1 = SigningKey.generate() # uses NIST192p
# vk1 = sk1.get_verifying_key()
# open("private1.pem","w").write(sk1.to_pem())
# open("public1.pem","w").write(vk1.to_pem())
# sk2 = SigningKey.generate() # uses NIST192p
# vk2 = sk2.get_verifying_key()
# print(len(sk1.to_string()), len(vk1.to_string()))

# open("private2.pem","w").write(sk2.to_pem())
# open("public2.pem","w").write(vk2.to_pem())
# while True:
# 	try:
# 		print("\nPublic key is:	"+ 	str(vk1))
# 		print("\nSecret key is:	"+ str(sk1))
# 		print("\nPublic key is:	"+ 	binascii.unhexlify(vk1.to_string())
# 		print("\nSecret key is:	"+ binascii.unhexlify(sk1.to_string()))
# 		print("\nPublic key is:	"+ str(vk2))
# 		print("\nSecret key is:	"+ str(sk2))
# 		message=raw_input('\nEnter your message here \n ...')
# 		sig = sk1.sign(message)
# 		print("signed message is: \n" + str(sig))
# 		print(vk1.verify(sig, message))
# 		print(vk2.verify(sig, message))
# 	except Exception as e:
# 		print (e)


# signature = sk.sign("message")
# assert vk.verify(signature, "message")

#############	BEGINING RSA CRYPTOSYSTEM HERE ############################
''' for Documentation read;
 https://www.dlitz.net/software/pycrypto/api/current/Crypto.PublicKey.RSA-module.html#importKey
https://www.dlitz.net/software/pycrypto/doc/#crypto-publickey-public-key-algorithms
'''
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from Crypto import Random

def gen_keys():
	try:
		rng = Random.new().read
		RSAkey = RSA.generate(1024, rng)
		sk=RSAkey.exportKey(format='PEM')
		pk=RSAkey.publickey().exportKey(format='PEM')
		sk_file=raw_input("\n file for storing secret key with .pem\n ")
		pk_file=raw_input("\n file for storing public key with .pem\n ")
		f1=open(sk_file,"w")
		f2=open(pk_file,"w")
		f1.write(sk)
		f2.write(pk)
		f1.close()
		f2.close()
	except Exception as e:
		print("error in generating keys...\a process aborted")
	
	# open("sk_file","w").write(sk)
	# open("pk_file","w").write(pk)
def sign():
	file_name=raw_input("\n name for the secret key file \n")
	temp_sk=""
	try:
		f=open(file_name)
		temp_sk=f.read()
		f.close()
	except Exception as e:
		print("\nerror in file reading...check the file\n")
	sk=RSA.importKey(temp_sk)
	msg=raw_input("\n your message \n")
	hash=SHA256.new(msg).hexdigest()
	en=sk.sign(hash,1)
	en_file_name=raw_input("\n file_name to store signature \n")
	try:
		f=open(en_file_name,"w")
		print("writing encyption as "+str(en[0]))
		f.write(str(en[0]))
		f.close
	except Exception as e:
		print("\n \a cannot write encryption to file, process aborted")

def verify():
	file_name=raw_input("\n name for the public key file \n")
	temp_pk=""
	try:
		f=open(file_name)
		temp_pk=f.read()
		f.close()
	except Exception as e:
		print("\nerror in file reading...check the file\n")
	pk=RSA.importKey(temp_pk)
	msg=raw_input("\n your message \n")
	hash=SHA256.new(msg).hexdigest()
	# en=sk1.sign(hash,1)
	en_file_name=raw_input("\n file_name to read signature \n")
	temp_en=0 #default value
	try:
		f=open(en_file_name)
		print("\n here \n")
		temp_en=long(f.read())
		# print(temp_en)3
		f.close
	except Exception as e:
		print("\n \a cannot read encrypted file, process aborted")
	en=(temp_en,)
	flag=pk.verify(hash,en)
	if flag:
		print("Verification successful")
	else:
		print("Verification failed")	

while True:
	print("\n Enter the choice")
	print("\n 1. Generate the key pairs")
	print("\n 2. encrypt a message")
	print("\n 3. verify an encryption")
	choice=int(raw_input())
	if (choice==1):
		gen_keys()
	elif (choice==2):
		sign()
	elif (choice ==3):
		verify()
	else:
		print("\n invalid input \a \n ")			



	

# temp_sk=open("private3.pem").read()
# msg=raw_input("\n your message \n")
# hash=SHA256.new(msg).hexdigest()
# sk1=RSA.importKey(temp_sk)
# print(sk1)
# en=sk1.sign(hash,1)
# print("\n"+str(en))
# open("encrypt.txt","w").write(str(en[0]))
# temp_pk=open("public3.pem").read()

# pk1=RSA.importKey(temp_pk)
# en2=(long(open("encrypt.txt").read()),)
# print(en2)
# print(pk1.verify(hash,en2))


