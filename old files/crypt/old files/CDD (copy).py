import ply.lex as lex
import ply.yacc as yacc
import os
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from __future__ import print_function


###########DEFINING CLASSSES FOR THE ABSTRACT SYNTAX TREE #################################33
class expression:
    def __init__(self,type,name, value = True):
        # expression could be a variable or a proposition
        self.type = type
        self.name=name
        self.value = value
        self.left=None
        self.right = None

    def Print(self):
        if(self.left != None):
            self.left.Print()
        print(self.name, end=' ')
        if(self.right != None):
            self.right.Print()
  
    def bin_op(self, exp1, exp2):
        if(self.type != "binop"):
            print("Error! operation not permitted")
        else:
            self.left=exp1
            self.right=exp2

    def says(self, P, s):
        if(self.type != "Says"):
            print("Error! operation not permitted")
        else:
            self.left=P
            self.left=s

    def evaluate(self):
        if(self.left==None or self.right==None):
            return self.value        
        elif (self.name == "AND"):
            return(self.left.evaluate() and self.right.evaluate())
        elif (self.name == "OR"):
            return(self.left.evaluate() or self.right.evaluate())
        elif (self.name == "IMPLIES"):
            return(not (self.left.evaluate()) or self.right.evaluate())
        else:
            return self.value            
                    
class Principal:
    def __init__(self,name):
        self.name=name
        self.key=None
    def add_key():
        if(self.key != None):
            print(self.name+" already have a key", end=" ")
            print(self.key)    
            print("error in adding new key")
        else:
            try:
                file_name=raw_input("\n please enter the name of file for the key of "+p[1]+" with .pem \n ")
                f=open(file_name)
                temp_key=f.read()
                key=RSA.importKey(temp_key)
                f.close()
                Keys[p[1]]=temp_key
            except Exception as e:
                print(" \n error in getting public key, aborting \n")    
                flag1=False
                



###################3 DEFINING LEXER ######################################        

tokens = (
#tokens    
	'PRINCIPLE',
	'AND',
	'OR',
	'IMPLIES',
	'NOT',
	'EQUIVALENT',
	'DERIVES',
	'SAYS',
	'SAYS_FOR',
	'CONTROLS',
	'CAN_ACCESS',
	'OBJECT',
	'LPAREN',
	'RPAREN',
	'COMMA',
	'TRUE',
	'FALSE',
	'EQUALS',
   	'SEMICOLON',
    'COLON',
#Names of rules starts here
	'AND_INTRODUCTION',
    'AND_ELIMINATION',
    'OR_INTRODUCTION',
    'OR_ELIMINATION',
    'MODES_PONES',
    'MODES_TOLLENS',
    'SYLLOGISM', 
    'ASSUME', 
    'EXCLUDED_MIDDLE',
    'UNITM',
    'BINDM',
    'FORALL',
    'DOT',
    'VARIABLE',
    'PROPOSITION',
    'TLAM',
    'TAPP',
    'L_SQ_BRACKET',
    'R_SQ_BRACKET',
    'F_SLASH',
    
)

reserved = {
	'can_access' : 'CAN_ACCESS',
	'says' : 'SAYS',
	'says_for' : 'SAYS_FOR',
	'controls' : 'CONTROLS',
	'TRUE':'true',
	'FALSE': 'false',
}


t_AND = r'&'#|r'and'|r'AND'
t_OR = r'\|'#|r'or'|r'OR'
t_IMPLIES = r'->'#|r'implies'
t_EQUIVALENT = r'<->'
t_DERIVES = r'\|-'
t_EQUALS=r'='
t_COLON=r':'

t_LPAREN  = r'\('
t_RPAREN  = r'\)'

t_L_SQ_BRACKET  = r'\['
t_R_SQ_BRACKET  = r'\]'
t_F_SLASH=r'/'
t_COMMA = r','
t_TRUE=r'True'
t_SEMICOLON=r';'
t_DOT = r'.' 

def t_NOT(t):
    r'not'
    t.type = reserved.get(t.value, 'NOT')
    return t 


def t_FORALL(t):
	r'forall'
	t.type = reserved.get(t.value, 'FORALL')
	return t

def t_VARIABLE(t):
	r'var'
	t.type = reserved.get(t.value, 'VARIABLE')
	return t

def t_PROPOSITION(t):
	r'prop'
	t.type = reserved.get(t.value, 'PROPOSITION')
	return t	    

def t_AND_INTRODUCTION(t):
    r'ANDI'
    t.type = reserved.get(t.value, 'AND_INTRODUCTION')
    return t 

def t_AND_ELIMINATION(t):
    r'ANDE'
    t.type = reserved.get(t.value, 'AND_ELIMINATION')
    return t

def t_OR_INTRODUCTION(t):
    r'ORI'
    t.type = reserved.get(t.value, 'OR_INTRODUCTION')
    return t 

def t_OR_ELIMINATION(t):
    r'ORE'
    t.type = reserved.get(t.value, 'OR_ELIMINATION')
    return t

def t_MODES_PONES(t):
    r'modes_pones'
    t.type = reserved.get(t.value, 'MODES_PONES')
    return t  

def t_MODES_TOLLENS(t):
    r'modes_tollens'
    t.type = reserved.get(t.value, 'MODES_TOLLENS')
    return t   

def t_SYLLOGISM(t):
    r'syl'
    t.type = reserved.get(t.value, 'SYLLOGISM')
    return t   

def t_ASSUME(t):
    r'assume'
    t.type = reserved.get(t.value, 'ASSUME')
    return t               

def t_EXCLUDED_MIDDLE(t):
    r'exm'
    t.type = reserved.get(t.value, 'EXCLUDED_MIDDLE')
    return t 

def t_UNITM(t):
    r'unitm'
    t.type = reserved.get(t.value, 'UNITM')
    return t  


def t_BINDM(t):
    r'bindm'
    t.type = reserved.get(t.value, 'BINDM')
    return t    


def t_TLAM(t):
    r'TLam'
    t.type = reserved.get(t.value, 'TLAM')
    return t    


def t_TAPP(t):
    r'TApp'
    t.type = reserved.get(t.value, 'TAPP')
    return t                 
#All specific token should be declared before this general token for principal and object    
def t_PRINCIPLE(t):
	r'[A-Z_][a-zA-Z_0-9]*'
	t.type = reserved.get(t.value, 'PRINCIPLE')
	return t  

def t_OBJECT(t):
	r'[a-z_][a-zA-Z_0-9]*'
	t.type = reserved.get(t.value, 'OBJECT')
	return t
    # print(t)

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()

#Building sets and dictionaries
principal=set()
proposition={ }
name=set()
assumed_prop=[]
Keys={}
var=set()

#Defining statement
def p_statement_expr(p):
    'statement : expression'
    if(p[1]!=None):
        proposition[p[1]]=True
        for e in proposition:
            e.Print()


def p_statement_rule(p):
    'statement : rule'
    p[0]=p[1]
    # print("here"+p[0])
    proposition[p[0]]=True
    print(proposition)


############################################################################################
##################### Grammar for expression ###############################################
############################################################################################

def p_expression_and(p):
    'expression : expression AND expression'
    p[0]=expression("binop", "AND")
    p[0].bin_op(p[1],p[3])
    p[0].Print()

def p_expression_or(p):
    'expression : expression OR expression'
    p[0]=expression("binop", "OR")
    p[0].bin_op(p[1],p[3])
    p[0].Print()

def p_expression_implies(p):
    p[0]=expression("binop", "IMPLIES")
    p[0].bin_op(p[1],p[3])
    p[0].Print()
    # proposition[p[0]]=True


def p_expression_says(p):
    'sub_expression : PRINCIPLE SAYS expression'
    if(proposition.get(p[3])):
        if(p[3].evaluate()):
            P=Principal(p[1])
            p[0]=expression("Says", "SAYS")
            p[0].says(P,p[3])
        return p[0]
    key=""
    # Getting public key for principal
    #Here one should add key for the principal P
    flag1=True
    if (p[1] in Keys):
        key=RSA.importKey(Keys[p[1]])
    else:
    msg=p[3]
    print("\n Message is \n ")
    print(msg)
    hash=SHA256.new(msg).hexdigest()
    flag = False #default value
    #verifying the validity of the expression
    if flag1:
        try:
            temp_file=raw_input("\n enter the file for the signature for the "+p[3]+"\n")
            f=open(temp_file)
            sign=(long(f.read()),)
            f.close()
            flag=key.verify(hash,sign)
            print(flag) # This verifcation process would not work until you fix the storage of the encryption   
        except Exception as e:
            print("Error in opening file for verifcation")
            flag=False    
    if (flag):
        print("\n verifcation successful\n ")
        principal.add(p[1])
        p[0] = (p[1]+ " SAYS " + p[3],True)
        p[0].Print()
    else:
        print("\n verification failed \n")    
        p[0] = (p[1]+ " SAYS " + p[3],False)
    # proposition[p[0]]=True

def p_expression_says_says(p):
    'sub_expression : PRINCIPLE SAYS sub_expression'
    if((p[3][1]) or (proposition.get(p[3][0]))):
        p[0] = (p[1]+ " SAYS " + p[3][0], True)
        return p[0]
    key=""
    # Getting public ket for principal
    flag1=True
    if (p[1] in Keys):
        key=RSA.importKey(Keys[p[1]])
    else:
        try:
            file_name=raw_input("\n please enter the name of file for the key of "+p[1]+" with .pem \n ")
            f=open(file_name)
            temp_key=f.read()
            key=RSA.importKey(temp_key)
            f.close()
            Keys[p[1]]=temp_key
        except Exception as e:
            print(" \n error in getting public key \n")    
            flag1=False
    msg=p[3][0]
    print("\n Message is \n ")
    print(msg)
    hash=SHA256.new(msg).hexdigest()
    flag = False #default value
    #verifying the validity of the expression
    if flag1:
        try:
            temp_file=raw_input("\n enter the file for the signature for the "+msg+"\n")
            f=open(temp_file)
            sign=(long(f.read()),)
            f.close()
            flag=key.verify(hash,sign)
            print(flag) # This verifcation process would not work until you fix the storage of the encryption   
        except Exception as e:
            print("Error in opening file for verifcation")
            flag=False    
    if (flag):
        print("\n verifcation successful\n ")
        principal.add(p[1])
        p[0] = (p[1]+ " SAYS " + p[3][0],True)
        p[0].Print()
    else:
        print("\n verification failed \n")    
        p[0] = (p[1]+ " SAYS " + p[3][0],False)


def p_expression_sub_expression(p):
    'expression : sub_expression'
    if (p[1][1]):
        p[0]=p[1][0]
        p[0].Print()

def p_expression_group(p):
    'expression : LPAREN expression RPAREN'
    # p[0] = '('+p[2]+')'   
    p[0]=p[2]
    p[0].Print()
    # proposition[p[0]]=True
 

def p_expression_object(p):
    'expression :  OBJECT'
    # print("It came here")
    p[0]=expression("proposition", p[1], True)
    p[0].Print()
    name.add(p[1])

def p_expression_true(p):
    'expression : TRUE'
    p[0]=str(True)
    name.add(True)
    # print (name)
    # print('above sentence was from truth expression')
    # proposition[p[0]]=True

def p_expression_false_object(p):
    'expression : expression IMPLIES FALSE'
    if(proposition[p[1]]==True):
        print("This would lead to contradiction") 
    else:
        # p[0]=p[1]
        proposition[p[1]]=True
    print (name)
    # print('above sentence was from false expression')    
def p_expression_not(p):
    'expression : NOT expression'
    p[0]=" NOT "+p[2]
    name.add(p[1])
    print (name)    

def p_expression_variable(p):
    'expression : VARIABLE OBJECT'
    p[0]=p[2]
    var.add(p[2])
    print ("\n variablle list is \n")        
    print(var)

def p_expression_forall(p):
    'expression : FORALL OBJECT DOT expression'
    if (p[2] in name):
    	print(p[2]+" is a proposition and not a variable. Choose different name \n \a ")
    	return
    if (p[4] in var):
    	print(p[4] + " is a variable and not a proposition. Choose different name \n \a ")	
    	return
    var.add(p[2])
    p[0]="FORALL "+p[2]+" DOT "+p[4]
    p[0].Print()
    print("\nlist of variables \n")
    print(var)
    

##################################################################################
####################   Writing rules from here ###################################
##################################################################################


def p_rule_EXCLUDED_MIDDLE(p):
    'rule : EXCLUDED_MIDDLE COLON NOT NOT expression'
    s=" NOT "+" NOT "+ p[5] 
    try:
        if(proposition[s]==True):
            p[0]=p[5]
            proposition.pop(s)
            print("double negation successful")
        else:
            print("double negation failed")    
            # proposition[p[0]]=True 
    except LookupError:
        print("double negation failed in proposition LookupError")


def p_rule_and_INTRODUCTION(p):
    'rule : AND_INTRODUCTION COLON expression COMMA expression'   
    try:
        if(proposition[p[3]]==True and proposition[p[5]]==True): 
            print("And INTRODUCTION successful")
            p[0]=p[3]+ " AND " + p[5]
            # proposition[p[0]]=True

        else:
            print("Derivation is not okay") 
    except LookupError:
        print("And INTRODUCTION failed")



def p_rule_and_elimination(p):
    'rule : AND_ELIMINATION COLON expression AND expression COMMA expression'   
    try:
        s=p[3]+" AND "+p[5]
        if(proposition[s]==True):
            if(p[7]==p[3] or p[7]==p[5]): #and ( p[1]=="true" and (proposition[p[3]==True):
                print("And elimination successful for: " +p[7])
                p[0]=p[7]
                # proposition[p[0]]=True
            else:
                print("proposition cannot be derived from the given statement")
        else:
            print("propostion not valid. And elimination failed") 
    except LookupError:
        print("And elimination failed for invalid proposition")  

def p_rule_modes_pones(p):
    'rule : MODES_PONES COLON expression IMPLIES expression COMMA expression'   
    try:
        s=p[3]+" IMPLIES "+p[5]
        if(proposition[s]==True):
            if(p[7]==p[3]): #and ( p[1]=="true" and (proposition[p[3]==True):
                print("Modes Pones  successfully derived: "+p[5])
                p[0]=p[5]
                # proposition[p[0]]=True
            else:
                print("proposition cannot be derived from the given statement")
        else:
            print("propostion not valid. Modes Pones failed") 
    except LookupError:
        print("MODES PONES failed for invalid proposition")  

def p_rule_modes_tollens(p):
    'rule : MODES_TOLLENS COLON expression IMPLIES expression'   
    try:
        s1=p[3]+" IMPLIES "+p[5]
        s2=" NOT "+p[5]
        if(proposition[s1]==True):
            if(proposition[s2]==True): 
                print("Modes MODES_TOLLENS  successfully derived NOT : "+p[3])
                try:
                    if(proposition[p[3]]==True):
                        print("\a ERROR: \n contradiction derived for: "+p[3])
                    else:
                        p[0]=" NOT " + p[3]
                except LookupError:
                    p[0]=" NOT "+p[3]        
                    # proposition[s2]=True
            else:
                print("proposition cannot be derived from the given statement")
        else:
            print("propostion not valid. Modes MODES TOLLENS failed") 
    except LookupError:
        print("MODES TOLLENS failed for invalid proposition") 


def p_rule_syllogism(p):
    'rule : SYLLOGISM COLON expression IMPLIES expression COMMA expression IMPLIES expression'   
    try:
        s1=p[3]+" IMPLIES "+p[5]
        s2=p[7]+" IMPLIES "+p[9]
        if((proposition[s1]==True) and (proposition[s2]== True)):
            if(p[5]==p[7]): 
                print("Modes SYLLOGISM  successfully derived: "+p[9])
                p[0]=p[3]+" IMPLIES "+p[9]
                # proposition[p[0]]=True 
            else:
                print("SYLLOGISM cannot be derived from the given statement")
        else:
            print("propostion not valid. SYLLOGISM failed") 
    except LookupError:
        print("SYLLOGISM failed for invalid proposition")            

def p_rule_or_introduction(p):
    'rule : OR_INTRODUCTION COLON expression IMPLIES expression OR expression'   
    try:
        if(proposition[p[3]]==True):
            if((p[5]==p[3]) or (p[7]==p[3])): 
                print("OR INTRODUCTION successfully derived: "+p[9])
                p[0]=p[5]+ " OR " + p[7]
                # proposition[p[0]]=True 
            else:
                print("OR INTRODUCTION cannot be derived for the given statement")
        else:
            print("propostion not valid. OR INTRODUCTION failed") 
    except LookupError:
        print("OR INTRODUCTION failed for invalid proposition") 


def p_rule_or_elemination1(p):
    'rule : OR_ELIMINATION COLON expression OR expression COMMA NOT expression'   
    try:
        s1=p[3]+" OR "+p[5]
        s2=" NOT "+ p[8]
        if((proposition[s1]==True) and (proposition[s2] == True)):
            if(p[8]==p[3]): 
                print("OR elimination successfully done for : "+p[5])
                p[0]=p[5]
                # proposition[p[0]]=True 
            elif(p[8]==p[5]): 
                print("OR elimination successfully done for : "+p[3])
                p[0]=p[3]
                # proposition[p[0]]=True 
            else:
                print("propostion not valid. OR elimination failed")
    except LookupError:
        print("OR elimination failed for invalid proposition") 

def p_rule_or_elemination2(p):
    'rule : OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression'   
    try:
        s1=p[3]+" OR "+p[5]
        if(proposition[s1]==True):
            if((p[7]==p[3]) and (p[11]==p[5]) and (p[9]==p[13])):
                s2=p[7]+ " IMPLIES " + p[9]
                s3=p[11]+ " IMPLIES " + p[13]
                if(s2 in proposition and s3 in proposition):
                    if(proposition[s2] and proposition[s3]) :
                        p[0]=p[9]
                        print("or elimination successfully generated: "+ p[9])
                    else:
                        print(" or elimination failed to verify the implication relations")
                else:
                    print("implication relation were not found for or elimination")
            elif((p[7]==p[5]) and (p[11]==p[3]) and (p[9]==p[13])):
                s2=p[7]+" IMPLIES "+ p[9]
                s3=p[11]+" IMPLIES "+ p[13]
                if(s2 in proposition and s3 in proposition):
                    if(proposition[s2] and proposition[s3]) :
                        p[0]=p[11]
                        print("or elimination successfully generated: "+ p[9])
                    else:
                        print(" or eliminationfailed to verify the implication relations")
                else:
                    print("implication relation were not found for or elimination")                
                        
                # proposition[p[0]]=True 
            else: 
                print("expression mismtatch")
                # proposition[p[0]]=True 
        else:
            print("propostion"+s1+" is not verified")
    except LookupError:
        print("OR elimination failed for invalid proposition") 
 

def p_rule_unitm(p):
    'rule : UNITM COLON expression COMMA PRINCIPLE SAYS expression'   
    try:
        if(proposition[p[3]]):
            if(p[3]==p[7]):
                p[0]=p[5]+" SAYS "+ p[7]
                print("unitm successful for "+ p[0])
            else:
                print("expression mismtatch in "+p[3]+" is not equal to "+ p[7])
        else:
            print("expression "+p[3]+" cannot be verified")
    except LookupError:
        print("unitm failed for invalid proposition") 

def p_rule_bindm(p):
    'rule : BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression seen_expression COMMA rule'  
    s=p[3]+" SAYS "+ p[5] 
    try:
        if((proposition[s]==True) and (p[5]==p[8])):
            if(p[3]==p[11].split(" ")[0]):
                p[0]=p[11]
                print('bindm successfully implemented for '+ p[11])
            else:
                print("bindm failed as principals were not same")
        else:
            print("bindm failed either verification failed or expressin mismatch")            
    except LookupError:
        print("unitm failed for invalid proposition") 

    if(assumed_prop.pop()):
        proposition[p[8]]=True
    else:
        proposition.pop(p[8])    

def p_rule_Implication_Introduction(p):
    'rule : ASSUME expression seen_expression COMMA  rule'   
    try:
        if(p[5] !=None):
            p[0]=p[2]+" IMPLIES "+p[5]
        else:
            print("propostion not valid. IMPLICATION INTRODUCTION failed") 
    except LookupError:
        print("IMPLICATION INTRODUCTION failed for invalid proposition")
    if(assumed_prop.pop()):
        proposition[p[2]]=True
    else:
        proposition.pop(p[2])    



def p_seen_expression(p):
    'seen_expression :'
    # print("Saw an expression = "+ p[-1])   # Access grammar symbol to left
    s=p[-1]
    if(s in proposition):
        assumed_prop.append(proposition[s])
    else :
        assumed_prop.append(False)    
    proposition[s]=True
    p[0]=p[-1]   


def p_rule_TLam(p):
    'rule : TLAM COLON expression COMMA FORALL OBJECT DOT expression'   
    try:
    	if (p[6] in proposition):
    		print(p[6]+" is a proposition and not a variable. Kindly change")
    		return
    	if (p[3]==p[8]):
    		if(proposition.get(p[8])):
	    		p[0]= "FORALL " +p[6]+" DOT " + p[3]
	    		print("TLam successfully done for "+p[3])
	    	else:
	    		print(" proposition not true. TLam failed")	
    	else:
    		print("error in TLam.  expression mismtatch")		
    except LookupError:
        print("TLAM elimination failed for invalid expression") 


def p_rule_TLapp(p):
    'rule : TAPP COLON FORALL OBJECT DOT expression COMMA expression L_SQ_BRACKET OBJECT F_SLASH OBJECT  R_SQ_BRACKET'   
    try:
    	if(p[4] in proposition):
    		print("variable is not correct. It is in the list of proposition. Kindly change")
    		return
    	if (p[4]!=p[12]):
    		print("Syntax error, variable mismtatch")
    		return
    	if (p[6]!=p[8]):
    		print("Syntax error, proposition mismtatch")
    		return			
    	if (free(p[4],p[6])): # check object p[4] should be free in expression	
    		p[0]=replace(p[10],p[12],p[6])

    	else:
    		print("error in TLam.  expression mismtatch")		
    except LookupError:
        print("TLAM elimination failed for invalid expression")


yacc.yacc()

while True:
        print("\n\n\n\n\n\n\n\n\a")
        print("--------------------------------------------------------------------------------------------------")
        print(" use & for AND \n use | for OR \n use -> for implication")
        print("rule  0: EXCLUDED_MIDDLE COLON NOT NOT expression")
        print("rule  1: AND_INTRODUCTION COLON (ANDI)expression COMMA expression ")    
        print("rule  2: AND_ELIMINATION COLON (ANDE) expression AND expression COMMA expression")
        print("rule  3: MODES_PONES COLON (modes_pones)expression IMPLIES expression COMMA expression")
        print("rule  4: MODES_TOLLENS (modes_tollens) COLON expression IMPLIES expression")
        print("rule  5: SYLLOGISM (syl) COLON expression IMPLIES expression COMMA expression IMPLIES expression")
        print("rule  6: OR_INTRODUCTION (ORI) COLON expression IMPLIES expression OR expression")
        print("rule  7: OR_ELIMINATION (ORE) COLON expression OR expression COMMA expression IMPLIES FALSE")
        print("rule  8: OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression")
        print("rule  9: UNITM COLON expression COMMA PRINCIPLE SAYS expression")
        print("rule 10: BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rule")
        print("rule 11: ASSUME expression COMMA rule")
        print("rule 12: TLAM COLON expression COMMA FORALL OBJECT DOT expression")
        print("rule 13: TAPP COLON FORALL OBJECT DOT expression COMMA expression L_SQ_BRACKET OBJECT F_SLASH OBJECT  R_SQ_BRACKET")
        print("--------------------------------------------------------------------------------------------------")
        print("\n\n\n")
        try:
            # s= data
      
            s = raw_input('statement > \n ')   # use input() on Python 3
            os.system("clear")
            print(s)
        except EOFError:
            break
        yacc.parse(s)
        # yacc.parse(" ras IMPLIES r1")   

def free(s,t):
	list1=t.split()
	flag=True
	for e in list1:
		if(s==e):
			flag=False
	return flag		

def replace(p,q,r):
	#one have to replace q by p in r
	list1=r.split()
	s=[p if x==q else x for x in r]
	r=''.join(str(e)+" " for e in s )
	l=len(r)
	r=r[0:l-1]
	return r

