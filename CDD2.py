from __future__ import print_function
import ply.lex as lex
import ply.yacc as yacc
import os
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256


###########DEFINING CLASSSES FOR THE ABSTRACT SYNTAX TREE #################################33
class expression:
    def __init__(self,type,name, value = None):
        # expression could be a variable or a proposition
        self.type = type
        self.name=name
        self.value = value
        self.left=None
        self.right = None

    def Print(self):
        if((self.left==None) and (self.right==None)):
            print(self.name, end=" ")
        else:    
            print(self.name, end=' (')
            if(self.left != None):
                self.left.Print()
                print(",", end=" ")
            if(self.right != None):
                self.right.Print()
            print(")", end=" ")
  
    def bin_op(self, exp1, exp2):
        if(self.type != "binop"):
            print("Error! operation not permitted")
        else:
            self.left=exp1
            self.right=exp2
    def NOT(self, exp):
        self.name="NOT"
        self.type="uni_op"
        self.right=exp
        val=self.right.evaluate()
        if(val!=None):
            self.value=not (val)
        else:
            self.val=None    
    def says(self, P, s):
        if(self.name != "SAYS"):
            print("Error! operation not permitted")
        else:
            self.left=P
            self.right=s

    def evaluate(self):
        if(self.value!=None):
                return self.value
        if(self.name == "NOT"):
            if(self.right.evaluate()!=None):
                self.value=not (self.right.evaluate())
            else:
                self.value=None
            return self.value      
        elif (self.name == "AND"):
            self.value=(self.left.evaluate() and self.right.evaluate())
            return self.value
        elif (self.name == "OR"):
            self.value=(self.left.evaluate() or self.right.evaluate())
            return self.value
        elif (self.name == "IMPLIES"):
            if ((self.right.evaluate()==True) or (self.left.evaluate()==False)):
                self.value=True
            elif((self.left.evaluate()==False) and (self.right.evaluate()==False)):
                self.value=False    
            else:
                self.value=None
            return self.value
        else:
            return None
            
    def to_string(self):
        s=""
        if(self.left != None):
            s=s+self.left.to_string()
            s=s+" "
        s=s+self.name
        # print(s)
        if (self.right != None):
            s=s+" "
            s = s+self.right.to_string()
        return s
    def is_equal(self, P):
        if((self.left!=None) and (P.left != None)):
            flag=self.left.is_equal(P.left)
        elif(self.left==None and P.left ==None):
            flag=True
        else:
            flag=False
            return flag

        if(self.name == P.name):
            flag = True
        else: 
            flag = False
            return flag
        if((self.right!=None) and (P.right != None)):
            flag=self.left.is_equal(P.left)
        elif(self.right==None and P.right ==None):
            flag=True
        else:
            flag=False
            return flag  
        return flag      


class Principal:
    def __init__(self,name, file_name):
        self.name=name
        self.key=None
        try:
            self.add_key(file_name)  
            principal[self.name]=self # we added a principal in the dictionary
            print("added new principal", end=" ")
        except Exception as e:
            print(" \n error in getting public key\n")
            print (e)
            print("Failed to add a new Principal")  
    def to_string(self):
        return self.name

    def add_key(self,file_name):
        if(self.key != None):
            print(self.name+" already have a key", end=" ")
            print(self.key)    
            print("error in adding new key")
        else:
            try:
                file_name=raw_input("\n please enter the name of file for the key of "+self.name+" with .pem \n ")
                f=open(file_name)
                temp_key=f.read()
                key=RSA.importKey(temp_key)
                f.close()
                self.key=temp_key
            except Exception as e:
                print(" \n error in getting public key, aborting \n")
    def add_key(self,file_name):
        if(self.key != None):
            print(self.name+" already have a key", end=" ")
            print(self.key)    
            print("error in adding new key")
        else:
                f=open(file_name)
                temp_key=f.read()
                key=RSA.importKey(temp_key)
                f.close()
                self.key=temp_key
                

    def has_key(self):
        if(self.key == None):
            return False
        else :
            return True
    def verify(self,msg,file_name):
        hash=SHA256.new(msg).hexdigest()
        flag = False
        if(self.key==None):
            print("verifcation failed due to invalid key")
            return False
        Key=RSA.importKey(self.key)
        try:
            f=open(file_name)
            sign=(long(f.read()),)
            f.close()
            flag=Key.verify(hash,sign)
            # print(flag)  
        except Exception as e:
            print("Error in opening file for verifcation")
            flag=False
        return flag                       

    
    def Print(self):
        print(self.name, end=" ")  


def get_principal(P_name):
    if(principal.get(P_name) == None):
            P=Principal(P_name)
            principal[P_name]=P # we added a principal in the dictionary
            # print("added new principal", end=" ")
            # P.Print()
            return P
    else:
        P=principal.get(P_name)
        return P  


###################3 DEFINING LEXER ######################################        

tokens = (
#tokens    
    'PRINCIPLE',
    'AND',
    'OR',
    'IMPLIES',
    'NOT',
    'EQUIVALENT',
    'DERIVES',
    'SAYS',
    'SAYS_FOR',
    'CONTROLS',
    'CAN_ACCESS',
    'OBJECT',
    'LPAREN',
    'RPAREN',
    'COMMA',
    'TRUE',
    'FALSE',
    'EQUALS',
    'SEMICOLON',
    'COLON',
    'ADD',
    'L_SQ_BRACKET',
    'R_SQ_BRACKET',
    'L_ANGLE_BRACKET',
    'R_ANGLE_BRACKET',
    'FILENAME',
#Names of rules starts here
    'AND_INTRODUCTION',
    'AND_ELIMINATION',
    'OR_INTRODUCTION',
    'OR_ELIMINATION',
    'MODES_PONES',
    'MODES_TOLLENS',
    'SYLLOGISM', 
    'ASSUME', 
    'EXCLUDED_MIDDLE',
    'UNITM',
    'BINDM',
    'FORALL',
    # 'DOT',
    'VARIABLE',
    'PROPOSITION',
    'TLAM',
    'TAPP',
    'F_SLASH',
    
)

reserved = {
    'can_access' : 'CAN_ACCESS',
    'says' : 'SAYS',
    'says_for' : 'SAYS_FOR',
    'controls' : 'CONTROLS',
}


t_AND = r'&'#|r'and'|r'AND'
t_OR = r'\|'#|r'or'|r'OR'
t_IMPLIES = r'->'#|r'implies'
t_EQUIVALENT = r'<->'
t_DERIVES = r'\|-'
t_EQUALS=r'='
t_COLON=r':'

t_LPAREN  = r'\('
t_RPAREN  = r'\)'

t_L_SQ_BRACKET  = r'\['
t_R_SQ_BRACKET  = r'\]'
t_L_ANGLE_BRACKET=r'<'
t_R_ANGLE_BRACKET=r'>'
t_F_SLASH=r'/'
t_COMMA = r','
# t_TRUE=r'True'
t_SEMICOLON=r';'
# t_DOT = r'.' 

def t_NOT(t):
    r'not'
    t.type = reserved.get(t.value, 'NOT')
    return t 
def t_FALSE(t):
    r'false'
    t.type = reserved.get(t.value, 'FALSE')
    return t 
def t_TRUE(t):
    r'true'
    t.type = reserved.get(t.value, 'TRUE')
    return t 

def t_ADD(t):
    r'ADD'
    t.type = reserved.get(t.value, 'ADD')
    return t 

def t_FORALL(t):
    r'forall'
    t.type = reserved.get(t.value, 'FORALL')
    return t

def t_VARIABLE(t):
    r'var'
    t.type = reserved.get(t.value, 'VARIABLE')
    return t

def t_PROPOSITION(t):
    r'prop'
    t.type = reserved.get(t.value, 'PROPOSITION')
    return t        

def t_AND_INTRODUCTION(t):
    r'ANDI'
    t.type = reserved.get(t.value, 'AND_INTRODUCTION')
    return t 

def t_AND_ELIMINATION(t):
    r'ANDE'
    t.type = reserved.get(t.value, 'AND_ELIMINATION')
    return t

def t_OR_INTRODUCTION(t):
    r'ORI'
    t.type = reserved.get(t.value, 'OR_INTRODUCTION')
    return t 

def t_OR_ELIMINATION(t):
    r'ORE'
    t.type = reserved.get(t.value, 'OR_ELIMINATION')
    return t

def t_MODES_PONES(t):
    r'modes_pones'
    t.type = reserved.get(t.value, 'MODES_PONES')
    return t  

def t_MODES_TOLLENS(t):
    r'modes_tollens'
    t.type = reserved.get(t.value, 'MODES_TOLLENS')
    return t   

def t_SYLLOGISM(t):
    r'syl'
    t.type = reserved.get(t.value, 'SYLLOGISM')
    return t   

def t_ASSUME(t):
    r'assume'
    t.type = reserved.get(t.value, 'ASSUME')
    return t               

def t_EXCLUDED_MIDDLE(t):
    r'exm'
    t.type = reserved.get(t.value, 'EXCLUDED_MIDDLE')
    return t 

def t_UNITM(t):
    r'unitm'
    t.type = reserved.get(t.value, 'UNITM')
    return t  


def t_BINDM(t):
    r'bindm'
    t.type = reserved.get(t.value, 'BINDM')
    return t    


def t_TLAM(t):
    r'TLam'
    t.type = reserved.get(t.value, 'TLAM')
    return t    


def t_TAPP(t):
    r'TApp'
    t.type = reserved.get(t.value, 'TAPP')
    return t                 
#All specific token should be declared before this general token for principal and object    
def t_FILENAME(t):
    r'[a-zA-Z_0-9][a-zA-Z_0-9]*["."][a-zA-Z_0-9][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'FILENAME')
    return t
def t_PRINCIPLE(t):
    r'[A-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'PRINCIPLE')
    return t  

def t_OBJECT(t):
    r'[a-z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'OBJECT')
    return t
    
# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()

#Building sets and dictionaries
principal={}
proposition={ }
name=set()
assumed_prop=[]


def p_statement_rule(p):
    'statement : rule'
    p[0]=p[1]
    if(p[1]!= None):
        proposition[p[0].to_string()]=True

def p_statement_command(p):
    'statement : command'
    if(p[1]!= None):
        print("Added: ", end=" ")
        p[1].Print()    


############################################################################################
##################### Grammar for expression ###############################################
############################################################################################
def p_command(p):
    'command : ADD COLON PRINCIPLE L_ANGLE_BRACKET FILENAME R_ANGLE_BRACKET'
    P=Principal(p[3],p[5])
  
def p_command_exp(p):
    'command : ADD COLON expression '
    try:
        if(p[3].name=='SAYS' and p[3].evaluate()==None):
            return None

        if((proposition.get(p[3].to_string())==False) or (p[3].evaluate()==False)):
            print("This expression is known to be false")

            p[0]=p[3]
            proposition[p[3].to_string()]=False

        if((proposition.get(p[3].to_string())==True) or (p[3].evaluate()==True)):
            print("This expression is known to be True")
            p[0]=p[3]
            proposition[p[3].to_string()]=True
        else:
            p[0]=p[3]
            proposition[p[3].to_string()]=None
    except Exception as e:
        print(" Error in getting expression")
        print(e)


def p_command_exp_t(p):
    'command : ADD COLON expression COMMA TRUE'
    try:
        if(p[3].name=='SAYS' and p[3].evaluate()==None):
            return None

        if((proposition.get(p[3].to_string())==False) or (p[3].evaluate()==False)):
            print(" This expression is known to be false")
            p[0]=p[3]
            proposition[p[3].to_string()]=False
        else:
            p[0]=p[3]
            proposition[p[3].to_string()]=True
    except Exception as e:
        print(" Error in getting expression")
        print(e)

def p_command_exp_f(p):
    'command : ADD COLON expression COMMA FALSE'
    try:
        if(p[3].name=='SAYS' and p[3].evaluate()==None):
            return None

        if((proposition.get(p[3].to_string())==True) or (p[3].evaluate()==True)):
            print("Error! This expression is known to be True")
            p[0]=p[3]
            proposition[p[3].to_string()]=True

        else:    
            p[0]=p[3]
            proposition[p[3].to_string()]=False
    except Exception as e:
        print(" Error in getting expression")
        print(e)  

def p_expression_and(p):
    'expression : expression AND expression'
    p[0]=expression("binop", "AND")
    p[0].bin_op(p[1],p[3])

def p_expression_implies(p):
    'expression : expression IMPLIES expression'
    p[0]=expression("binop", "IMPLIES")
    p[0].bin_op(p[1],p[3])

def p_expression_or(p):
    'expression : expression OR expression'
    p[0]=expression("binop", "OR")
    p[0].bin_op(p[1],p[3])

def p_expression_says(p):
    'expression : PRINCIPLE SAYS expression L_ANGLE_BRACKET FILENAME R_ANGLE_BRACKET'
    try:
        P=principal.get(p[1])
        if(P==None):
            print("no such principal found. Error! \a")    
                    
        if(proposition.get(p[3].to_string())):
            if(proposition.get(p[3].to_string())):
                flag =True
            else:
                flag=P.verify(p[3].to_string(),p[5])    
        else:
            flag = P.verify(p[3].to_string(),p[5])

        p[0]=expression("binop", "SAYS",flag)
        p[0].says(P,p[3])
    except Exception as e:
        print("failed to add the expression")   

def p_expression_says_no_verification(p):
    'expression : PRINCIPLE SAYS expression'
    P=principal.get(p[1])

    try:
        if(P==None):
            print("no such principal found. Error! \a")
        
        else: 
            if(proposition.get(p[3].to_string())):
                flag = True   
            elif(proposition.get(p[3].to_string())==False):
                flag = False
            else:
                flag = None    

            p[0]=expression("binop", "SAYS",flag)
            p[0].says(P,p[3])

    except Exception as e:
        print(" Failed to add the expression")
        print(e)

def p_expression_group(p):
    'expression : LPAREN expression RPAREN'
    p[0]=p[2]
    p[0].Print()
   
 

def p_expression_object(p):
    'expression :  OBJECT'
    p[0]=expression("proposition", p[1])
    name.add(p[1])

def p_expression_true(p):
    'expression : TRUE'
    p[0]=expression("True","True",True)
    name.add(True)

def p_expression_not_not(p):
    'expression : NOT NOT expression'

    s1=expression("uni_op", "NOT")
    s2=expression("uni_op", "NOT")
    s1.NOT(p[3])
    s2.NOT(s1)
    p[0]=s2

def p_expression_not(p):
    'expression : NOT expression'
    p[0]=expression("uni_op","NOT")
    p[0].NOT(p[2])
    p[0].value

##################################################################################
####################   Writing rules from here ###################################
##################################################################################


def p_rule_EXCLUDED_MIDDLE(p):
    'rule : EXCLUDED_MIDDLE COLON expression'
    if(proposition.get(p[3].to_string())!=True):
        print("double negation failed for incorrect proposition")
    else:    
        try:
            if((p[3].name =="NOT") and (p[3].right.name==("NOT"))):
                p[0]=p[3].right.right
                print("double negation successful")
            else:
                print("double negaiton failed")    
        except Exception as e:
            print("double negation failed for improper expression")

def p_rule_and_INTRODUCTION(p):
    'rule : AND_INTRODUCTION COLON expression COMMA expression'   
    try:
        if(proposition.get(p[3].to_string()) and proposition.get(p[5].to_string())): 
            print("And INTRODUCTION successful")
            p[0]=expression("binop","AND")
            p[0].bin_op(p[3],p[5])
            p[0].Print()

        else:
            print("Derivation is not okay. One of the expression is false") 
    except Exception as e:
        print("And INTRODUCTION failed for improper expression")
        print(e)
            



def p_rule_and_elimination(p):
    'rule : AND_ELIMINATION COLON expression COMMA expression'   
    try:
        if(p[3].name !="AND"):
            print("And elimination failed for improper expression")
        if(proposition.get(p[3].to_string())!= True):
            print("AND elimination failed due to improper proposition")
            p[0]=None
            
        else:
            if(p[3].left.to_string()==p[5].to_string()):
                p[0]=p[5]
                print("And elimination successful for ", end=" ")
                p[5].Print()
            elif(p[3].right.to_string()==p[5].to_string()):
                p[0]=p[5]
                print("And elimination successful for ", end=" ")
                p[5].Print()
        
    except Exception as e:
        print("And elimination failed for invalid proposition")  
        print(e)

def p_rule_modes_pones(p):
    'rule : MODES_PONES COLON expression IMPLIES expression COMMA expression'   
    try:
        s=expression("binop", "IMPLIES")
        s.bin_op(p[3],p[5])
        if(proposition[s.to_string()]==True):
            if(p[7].is_equal(p[3])): 
                print("Modes Pones  successfully derived: ") 
                p[0]=p[5]
                p[0].Print()
            else:
                print("proposition cannot be derived from the given statement")
        else:
            print("propostion not valid. Modes Pones failed") 
    except Exception as e:
        print("MODES PONES failed for invalid proposition") 
        print(e) 

def p_rule_modes_tollens(p):
    'rule : MODES_TOLLENS COLON expression IMPLIES expression'   
    try:
        s1=expression("binop", "IMPLIES")
        s1.bin_op(p[3],p[5])
        s2=expression("uni_op","NOT")
        s2.NOT(p[5])
        if(proposition[s1.to_string()]==True):
            if(proposition.get(s2.to_string())==True): 
                print("MODES_TOLLENS  successfully derived NOT : "+p[3].to_string())
                try:
                    if(proposition[p[3].to_string()]==False):
                        print("\a ERROR: \n contradiction derived for: "+p[3].to_string())
                    else:
                        p[0]=expression("uni_op","NOT")
                        p[0].NOT(p[3])
                except LookupError:
                    p[0]=expression("uni_op","NOT")
                    p[0].NOT(p[3])      
            else:
                print("proposition cannot be derived from the given statement")
        else:
            print("propostion not valid. Modes MODES TOLLENS failed") 
    except LookupError:
        print("MODES TOLLENS failed for invalid proposition") 


def p_rule_syllogism(p):
    'rule : SYLLOGISM COLON expression IMPLIES expression COMMA expression IMPLIES expression'   
    try:
        s1=expression("binop", "IMPLIES")
        s1.bin_op(p[3],p[5])
        s2=expression("binop", "IMPLIES")
        s2.bin_op(p[7],p[9])
        if((proposition[s1.to_string()]==True) and (proposition[s2.to_string()]== True)):
            if(p[5].is_equal(p[7])): 
                print("Modes SYLLOGISM  successfully derived: "+p[3].to_string() +" IMPLIES "+ p[9].to_string())
                p[0]=expression("binop", "IMPLIES")
                p[0].bin_op(p[3],p[9])
            else:
                print("SYLLOGISM cannot be derived from the given statement")
        else:
            print("propostion not valid. SYLLOGISM failed") 
    except LookupError:
        print("SYLLOGISM failed for invalid proposition")            

def p_rule_or_introduction(p):
    'rule : OR_INTRODUCTION COLON expression COMMA expression OR expression'   
    try:
        if(proposition.get(p[3].to_string())):
            if((p[5].is_equal (p[3])) or (p[7].is_equal(p[3]))): 
                print("OR INTRODUCTION successfully derived: ")
                p[0]=expression("binop","OR")
                p[0].bin_op(p[5],p[7])
            else:
                print("OR INTRODUCTION cannot be derived for the given statement")
        else:
            print("propostion not valid. OR INTRODUCTION failed") 
    except Exception as e:
        print("OR INTRODUCTION failed for invalid proposition") 
    
def p_rule_or_elemination1(p):
    'rule : OR_ELIMINATION COLON expression OR expression COMMA NOT expression'   
    try:
        s1=expression("binop","OR")
        s1.bin_op(p[3],p[5])
        s3=expression("binop","OR")
        s3.bin_op(p[5],p[3])
        s2=expression("uni_op", "NOT")
        s2.NOT(p[8])
        if(((proposition.get(s1.to_string())==True) or (proposition.get(s3.to_string())==True)) and ((proposition.get(s2.to_string())) or (proposition.get(p[8].to_string())==False))):
            if(p[8].is_equal(p[3])): 
                print("OR elimination successfully done for : "+p[5].to_string())
                p[0]=p[5]
            elif(p[8].is_equal(p[5])): 
                print("OR elimination successfully done for : "+p[3].to_string())
                p[0]=p[3]
            else:
                print("propostion not valid. OR elimination failed")
        else:
            print("Or elimination failed")
    except Exception as e:
        print("OR elimination failed for invalid proposition") 

def p_rule_or_elemination2(p):
    'rule : OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression'   
    try:
        s1=expression("binop","OR")
        s1.bin_op(p[3],p[5])
        if(proposition[s1.to_string()]==True):
            if((p[7].is_equal(p[3])) and (p[11].is_equal(p[5])) and (p[9].is_equal(p[13]))):
                s2=expression("binop","IMPLIES")
                s2.bin_op(p[7],p[9])
                s3=expression("binop","IMPLIES")
                s3.bin_op(p[11],p[13])
                if(proposition.get (s2.to_string()) and proposition.get(s3.to_string())):
                    p[0]=p[9]
                    print("or elimination successfully generated: "+ p[9].to_string())
                else:
                    print(" or elimination failed to verify the implication relations")
            elif((p[7].is_equal(p[5])) and (p[11].is_equal(p[3])) and (p[9].is_equal(p[13]))):
                s2=expression("binop","IMPLIES")
                s2.bin_op(p[7],p[9])
                s3=expression("binop","IMPLIES")
                s3.bin_op(p[11],p[13])
                if(proposition.get (s2.to_string()) and proposition.get(s3.to_string())):
                    p[0]=p[9]
                    print("or elimination successfully generated: "+ p[9].to_string())
                else:
                    print(" or elimination failed to verify the implication relations")            
                        
            else: 
                print("expression mismtatch")
        else:
            print("propostion"+s1.to_string()+" is not verified")
    except LookupError:
        print("OR elimination failed for invalid proposition") 
 

def p_rule_unitm(p):
    'rule : UNITM COLON expression COMMA PRINCIPLE SAYS expression'    
    P=principal.get(p[5])
    try:
        if(P==None):
            print("no such principal found. Error! \a")
        
        else: 
            if(proposition.get(p[3].to_string()) and (p[3].is_equal(p[7]))):
                p[0]=expression("binop", "SAYS",True)
                p[0].says(P,p[3])

    except Exception as e:
        print(" Failed to add the expression")        


def p_rule_bindm(p):
    'rule : BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rule'  
    P=principal.get(p[3])
    if (P== None):
        print("No such Principal found. Error! \a")
        return
    s=expression("bin_op","SAYS")
    s.says(P,p[5])
    val=p[8].evaluate()
    p[8].value=True
    try:
        if((proposition.get(s.to_string())) and (p[5].is_equal(p[8]))):
            if(p[10].left == P):
                p[0]=p[10]
                print('bindm successfully implemented for ')
                p[0].Print()
            else:
                print("bindm failed as principals were not same")
        else:
            print("bindm failed either verification failed or expression mismatch")            
    except LookupError:
        print("unitm failed for invalid proposition") 


    p[8].value=val
def p_rule_Implication_Introduction(p):
    'rule : ASSUME expression seen_expression COMMA rule'   
    try:
        if(p[5] !=None):
            if(p[5].evaluate()):
                p[0]=expression("binop", "IMPLIES")
                p[0].bin_op(p[2], p[5])
            else:
                print("propostion is false. IMPLICATION INTRODUCTION failed")
        else:
            print("propostion not valid. IMPLICATION INTRODUCTION failed") 
    except LookupError:
        print("IMPLICATION INTRODUCTION failed for invalid proposition")
    proposition[p[2].to_string()]=assumed_prop.pop()    


def p_seen_expression(p):
    'seen_expression :'
    s=p[-1].to_string()
    assumed_prop.append(proposition.get(s))   
    proposition[s]=True
    p[0]=p[-1]   


yacc.yacc()

while True:
        print("\n\n\n\n\n\n\n\n\a")
        print("--------------------------------------------------------------------------------------------------")
        print(" use & for AND \n use | for OR \n use -> for implication")
        print("rule  0: EXCLUDED_MIDDLE (exm) COLON NOT NOT expression")
        print("rule  1: AND_INTRODUCTION COLON (ANDI)expression COMMA expression ")    
        print("rule  2: AND_ELIMINATION COLON (ANDE) expression AND expression COMMA expression")
        print("rule  3: MODES_PONES COLON (modes_pones)expression IMPLIES expression COMMA expression")
        print("rule  4: MODES_TOLLENS (modes_tollens) COLON expression IMPLIES expression")
        print("rule  5: SYLLOGISM (syl) COLON expression IMPLIES expression COMMA expression IMPLIES expression")
        print("rule  6: OR_INTRODUCTION (ORI) COLON expression COMMA L_BRACKET expression R_BRACKET OR L_BRACKET expression R_BRACKET")
        print("rule  7: OR_ELIMINATION (ORE) COLON expression OR expression COMMA expression IMPLIES FALSE")
        print("rule  8: OR_ELIMINATION COLON expression OR expression COMMA expression IMPLIES expression COMMA expression IMPLIES expression")
        print("rule  9: UNITM COLON expression COMMA PRINCIPLE SAYS expression")
        print("rule 10: BINDM COLON PRINCIPLE SAYS expression COMMA ASSUME expression COMMA rule")
        print("rule 11: ASSUME expression COMMA rule")
        print("rule 12: TLAM COLON expression COMMA FORALL OBJECT DOT expression")
        print("rule 13: TAPP COLON FORALL OBJECT DOT expression COMMA expression L_SQ_BRACKET OBJECT F_SLASH OBJECT  R_SQ_BRACKET")
        print("--------------------------------------------------------------------------------------------------")
        print("\n\n\n")
        try:
            # s= data
      
            s = raw_input('statement > \n ') # use input() on Python 3
            os.system("clear")
            if(s=="a"):
                  print(proposition)
                  print(principal)
            print(s)
        except EOFError:
            break
        
        yacc.parse(s)
        print("propositions")
        print("--------------------------------------------------------------------------")
        for e in sorted(proposition.iterkeys()):
            print(e, end="   ")
            print(proposition.get(e))

        print(" \n\n\n\n\n")
        print("Principals")
        print("----------------------------------------------------------------------------")
        for e in principal:
            print(e, end="   ")
            print(principal.get(e))

        print("----------------------------------------------------------------------------")










